package org.barzensuit.client;

public class Main {

	public static int EVTID_ON_CLICK = 0x00001000;
	
	public static void main(String[] args) {
		System.out.println("launching barzensuit client...");
		Context context = new Context();
		Event event = new Event();
		EventMonitor monitor = new EventMonitor();
		context.setId(EVTID_ON_CLICK);
		monitor.setContext(context);
		monitor.setEvent(event);
		monitor.startup();
		monitor.triggered();
		monitor.shutdown();
		System.out.println("terminating barzensuit client");
	}

}
