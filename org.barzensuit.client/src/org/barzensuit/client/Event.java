package org.barzensuit.client;

import java.util.ArrayList;
import java.util.Iterator;

public class Event {
	
	/**
	 * @var List<IEventListener> Listeners attached to this event.
	 */
	private ArrayList<IEventListener> listeners;
	
	public Event() {
		super();
		listeners = new ArrayList<IEventListener>();
	}
	
	public synchronized void addListener(IEventListener listener) {
		listeners.add(listener);
	}
	
	public synchronized void removeListener(IEventListener listener) {
		listeners.remove(listener);
	}
	
	public synchronized void trigger() {
		for (Iterator<IEventListener> i = listeners.iterator(); i.hasNext();)
			((IEventListener) i.next()).triggered();
	}

}
