package org.barzensuit.client;

public class Context {

	/**
	 * @var int id Unique id of encapsulated context.
	 */
	private int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Context() {
		super();
	}

}
