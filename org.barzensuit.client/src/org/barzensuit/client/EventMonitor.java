package org.barzensuit.client;

public class EventMonitor implements IEventListener {
	/**
	 * 
	 */
	private Event event;
	
	/**
	 * 
	 */
	private Context context;
	
	/**
	 * 
	 */
	@Override
	public void triggered() {
		System.out.println("Event triggered. Context = " + context.getId());
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public void shutdown() {
		event.removeListener(this);
	}
	
	public void startup() {
		event.addListener(this);
	}
}
